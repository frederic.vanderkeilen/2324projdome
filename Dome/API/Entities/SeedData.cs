﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;

namespace API.Entities
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using UserManager<User> _userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            using RoleManager<Role> _roleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();
            using (var context = new DataBaseContext(
                   serviceProvider.GetRequiredService<
                       DbContextOptions<DataBaseContext>>()))
            {
                if (!context.Roles.Any())
                {
                    _ = _roleManager.CreateAsync(new Role { Name = "admin", Description = "admin is boss" }).Result;
                    _ = _roleManager.CreateAsync(new Role { Name = "user", Description = "user uses" }).Result;
                }
            }
        }
    }
}


