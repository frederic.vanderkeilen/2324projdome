﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class User : IdentityUser<Guid>
    {
        //[Required]
        //[RegularExpression(@"(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$")]
        //public Guid Id { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string LastName { get; set; }
        //public virtual Guid?  TaskSectionId { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<TaskSection>? TaskSections { get; set; }
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
