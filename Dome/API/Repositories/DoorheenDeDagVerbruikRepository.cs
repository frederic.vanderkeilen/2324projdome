﻿using Globals;
using API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Globals.Models.User;
using Globals.Models.TaskSection;
using Globals.Models.TaskItem;
using System;
using API.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace API.Repositories
{
	public interface IDoorheenDeDagVerbruikRepository
	{
		Task<List<DoorheenDeDagVerbruik>> GetEnergyUsage();
	}
	public class DoorheenDeDagVerbruikRepository : IDoorheenDeDagVerbruikRepository
	{
		private readonly DataBaseContext _context;
		public DoorheenDeDagVerbruikRepository(DataBaseContext context)
		{
			_context = context;
		}
		public async Task<List<DoorheenDeDagVerbruik>> GetEnergyUsage()
		{
			return await _context.doorheendedagverbruik
			   .Select(x => new DoorheenDeDagVerbruik
			   {
				   Id = x.Id,
				   DagVerbruik = x.DagVerbruik,
				   NachtVerbruik = x.NachtVerbruik,
				   TotaalVerbruik = x.TotaalVerbruik,
				   Timestamp = x.Timestamp
			   })
			   .AsNoTracking()
			   .ToListAsync();
		}
	}
}
