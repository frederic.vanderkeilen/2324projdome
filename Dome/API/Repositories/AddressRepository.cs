﻿using Globals;
using API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Globals.Models.User;
using Globals.Models.TaskSection;
using Globals.Models.TaskItem;
using System;
using API.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace API.Repositories
{
	public interface IAddressRepository
	{
		Task<Address> GetAddress();
		Task<Address> PostAddress(Address address);
	}
	public class AddressRepository : IAddressRepository
	{
		private readonly DataBaseContext _context;
		public AddressRepository(DataBaseContext context)
		{
			_context = context;
		}
		public async Task<Address> GetAddress()
		{
			Address? address = await _context.address
			   .Select(x => new Address
			   {
				   Id = x.Id,
				   IvagoStreet = x.IvagoStreet,
				   Number = x.Number
			   })
			   .AsNoTracking()
			   .FirstOrDefaultAsync();
			return address;
		}

		public async Task<Address> PostAddress(Address address)
		{
			Address _address = new Address
			{
				Id = address.Id,
				IvagoStreet = address.IvagoStreet,
				Number = address.Number
			};

			await _context.address.AddAsync(address);
			await _context.SaveChangesAsync();

			return _address;
		}
	}
}
