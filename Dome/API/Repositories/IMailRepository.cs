﻿using Globals;

namespace API.Repositories
{
	public interface IMailRepository
	{
		Task<MailItem> GetMail();
	}
}
