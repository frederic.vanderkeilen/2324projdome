﻿using API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using Globals.Models.TaskItem;
using Globals.Models.TaskSection;

namespace API.Repositories
{
    public class TaskSectionRepository : ITaskSectionRepository
    {
        private readonly DataBaseContext _context;

        public TaskSectionRepository(DataBaseContext context)
        {
            _context = context;
        }

        public async Task<string> DeleteTaskSection(Guid id)
        {
			try
			{
				TaskSection? taskSection = await _context.TaskSections.FindAsync(id);
				_context.Remove(taskSection);
				await _context.SaveChangesAsync();
				return "y";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}

		//public async Task<GetTaskSectionModel> GetTaskSection(Guid id)
		//      {
		//          var taskSection = await _context.TaskSections
		//              .Select(x => new GetTaskSectionModel
		//              {
		//                  Id = x.Id,
		//                  Name = x.Name,
		//                  GetTaskItemModels = x.TaskItems.Select(x => new GetTaskItemModel
		//                  {
		//                      Id = x.Id,
		//                      Name = x.Name

		//                  }).ToList()
		//              })
		//              .AsNoTracking()
		//              .FirstOrDefaultAsync(x => x.UserId == id);

		//          return taskSection; 
		//      }

		public async Task<List<GetTaskSectionModel>> GetTaskSection(Guid userId)
		{
			var taskSections = await _context.TaskSections
				.Include(ts => ts.TaskItems) // Include the related TaskItems
				.AsNoTracking()
				.Where(ts => ts.UserId == userId) // Filter by user ID
				.ToListAsync();

			if (!taskSections.Any())
			{
				return new List<GetTaskSectionModel>(); // Return an empty list if no task sections are found
			}

			var result = taskSections.Select(ts => new GetTaskSectionModel
			{
				Id = ts.Id,
				Name = ts.Name,
				GetTaskItemModels = ts.TaskItems.Select(ti => new GetTaskItemModel
				{
					Id = ti.Id,
					Name = ti.Name
				}).ToList()
			}).ToList();

			return result;
		}


		public async Task<List<GetTaskSectionModel>> GetTaskSections()
        {
            return await _context.TaskSections
               .Select(x => new GetTaskSectionModel
               {
                   Id = x.Id,
                   Name = x.Name,
				   UserId = x.UserId,
                   GetTaskItemModels = x.TaskItems.Select(x => new GetTaskItemModel
                   {
                       Id = x.Id,
                       Name = x.Name

                   }).ToList()
               })
               .AsNoTracking()
               .ToListAsync();

           
        }

     

        public async Task<GetTaskSectionModel> PostTaskSection(PostTaskSectionModel postTaskSectionModel)
        {
            TaskSection taskSection = new TaskSection
            {
                Name = postTaskSectionModel.Name,
                UserId = postTaskSectionModel.UserId,
                TaskItems = new List<TaskItem>()
            };

            await _context.TaskSections.AddAsync(taskSection);
            await _context.SaveChangesAsync();

            return new GetTaskSectionModel
            {
                Id = taskSection.Id,
                Name = taskSection.Name
            };
        }

        public async Task PutTaskSection(Guid id, PutTaskSectionModel putTaskSectionModel)
        {
            TaskSection taskSection = await _context.TaskSections.FindAsync(id);
            taskSection.Name = putTaskSectionModel.Name;
            _context.Update(taskSection);
            await _context.SaveChangesAsync();

        }
    }
}
