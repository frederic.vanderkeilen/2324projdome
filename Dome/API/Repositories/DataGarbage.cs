﻿using System.Net;
using System.Net.Http;

namespace API.Repositories
{
    public class DataGarbage
    {
        private readonly HttpClient _httpClient;
        private readonly CookieContainer _cookieContainer;
        public string sessionCookie { get; private set; }

        public DataGarbage()
        {
            _cookieContainer = new CookieContainer();
            var handler = new HttpClientHandler
            {
                CookieContainer = _cookieContainer
            };
            _httpClient = new HttpClient(handler);
        }
        public async Task GetSessionCookieAsync(string ivagoStreet, string number)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://www.ivago.be/nl/particulier/afval/ophaling");

            request.Headers.Add("Accept", "*/*");
            request.Headers.Add("Cookie", "cookie-agreed-version=1.0.0; cookie-agreed-categories=[%22necessary_cookies%22]; cookie-agreed=2; Drupal_visitor_current_target=particulier");
            request.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36");

            var rawData = $"garbage_type=&ivago_street={ivagoStreet}&number={number}&form_build_id=form-V07K3zmJv96Lw_5iuDKcqZeb0ZsIzzNFOMnok_Q3CSg&form_id=garbage_address_form&op=Bekijk";
            var content = new StringContent(rawData, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");
            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            if (response.Headers.TryGetValues("Set-Cookie", out var setCookieHeaders))
            {
                foreach (var setCookieHeader in setCookieHeaders)
                {
                    if (setCookieHeader.StartsWith("SSESScb58c787e2bf9fd407e19d9a2a0995a5"))
                    {
                        var sessionCookie = setCookieHeader.Split(';').FirstOrDefault();
                        sessionCookie = sessionCookie.Replace("SSESScb58c787e2bf9fd407e19d9a2a0995a5=", "");
                        this.sessionCookie = sessionCookie;
                    }
                }
            }
        }
    }
}
