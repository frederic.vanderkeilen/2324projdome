﻿using Microsoft.AspNetCore.Mvc;
using Globals.Models.TaskItem;
using Globals.Models.User;

namespace API.Repositories
{
    public interface ITaskItemRepository
    {
        Task<List<GetTaskItemModel>> GetTaskItems();
        Task<GetTaskItemModel> GetTaskItem(Guid id);
        Task<GetTaskItemModel> PostTaskItem(PostTaskItemModel postTaskItemModel);
        Task PutTaskItem(Guid id, PutTaskItemModel putTaskItemModel);
        Task PatchTaskItem(string id, PatchTaskItemModel patchTaskItemModel);
        Task<string> DeleteTaskItem(Guid id);
    }
}
