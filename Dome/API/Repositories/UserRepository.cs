﻿using API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Globals.Models.User;
using Globals.Models.TaskSection;
using Globals.Models.TaskItem;
using System;
using API.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace API.Repositories
{
	public class UserRepository : IUserRepository
	{
		private readonly DataBaseContext _context;
		private readonly UserManager<User> _userManager;
		private readonly SignInManager<User> _signInManager;
		private readonly AppSettings _appSettings;
		private readonly IHttpContextAccessor _httpContextAccessor;
		private readonly RoleManager<Role> _roleManager;
		//private readonly ClaimsPrincipal _person;

		public UserRepository(DataBaseContext context, UserManager<User> userManager, SignInManager<User> signInManager, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor)
		{
			_context = context;
			_userManager = userManager;
			_signInManager = signInManager;
			_appSettings = appSettings.Value; // ????? 
			_httpContextAccessor = httpContextAccessor;
			//_person = httpContextAccessor.HttpContext.User;
		}

		public async Task<PostAuthenticationResponseModel> Authenticate(PostAuthenticationRequestModel postAuthenticationRequestModel, string ipAddress)
		{
			User user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == postAuthenticationRequestModel.Username);

			if (user == null)
			{
				throw new Exception("Incorrect email");
			}

			SignInResult signInresult = await _signInManager.CheckPasswordSignInAsync(user, postAuthenticationRequestModel.Password, lockoutOnFailure: false);
			if (!signInresult.Succeeded)
			{
				throw new Exception("Incorrect password");
			}
			if (user.RefreshTokens == null)
			{
				user.RefreshTokens = new List<RefreshToken>();
			}
			string jwtToken = await GenerateJwtToken(user);
			RefreshToken refreshToken = GenerateRefreshToken(ipAddress);
			refreshToken.ReplacedByToken = "dummyNull";
			refreshToken.RevokedByIp = ipAddress;
			user.RefreshTokens.Add(refreshToken);

			await _userManager.UpdateAsync(user);

			return new PostAuthenticationResponseModel
			{
				Id = user.Id.ToString(),
				FirstName = user.FirstName,
				LastName = user.LastName,
				UserName = user.UserName,
				JwtToken = jwtToken,
				RefreshToken = refreshToken.Token,
				Roles = await _userManager.GetRolesAsync(user)
			};
		}

		public async Task DeactivateToken(string token, string ipAddress)
		{
			User user = await _userManager.Users.FirstOrDefaultAsync(x => x.RefreshTokens.Any(t => t.Token == token));

			if (user == null)
			{
				throw new Exception("No user found with this token");
			}

			RefreshToken refreshToken = user.RefreshTokens.Single(x => x.Token == token);

			if (!refreshToken.IsActive)
			{
				throw new Exception("Refresh token is expired");
			};

			refreshToken.Revoked = DateTime.UtcNow;
			refreshToken.RevokedByIp = ipAddress;

			await _userManager.UpdateAsync(user);
		}

		public async Task<PostAuthenticationResponseModel> RenewToken(string token, string ipAddress)
		{
			User user = await _userManager.Users.FirstOrDefaultAsync(x => x.RefreshTokens.Any(t => t.Token == token));
			if (user == null)
			{
				throw new Exception("No users found with this token");
			}

			RefreshToken refreshToken = user.RefreshTokens.Single(x => x.Token == token);

			if (!refreshToken.IsActive)
			{
				throw new Exception("Refresh token expired");
			};

			RefreshToken newRefreshToken = GenerateRefreshToken(ipAddress);
			refreshToken.RevokedByIp = ipAddress;
			refreshToken.ReplacedByToken = newRefreshToken.Token;

			string jwtToken = await GenerateJwtToken(user);

			user.RefreshTokens.Add(newRefreshToken);

			await _userManager.UpdateAsync(user);

			return new PostAuthenticationResponseModel
			{
				Id = user.Id.ToString(),
				FirstName = user.FirstName,
				LastName = user.LastName,
				UserName = user.UserName,
				JwtToken = jwtToken,
				RefreshToken = newRefreshToken.Token,
				Roles = await _userManager.GetRolesAsync(user)
			};
		}

		public async Task<IdentityResult> DeleteUser(string id)
		{
			User? user = await _userManager.FindByIdAsync(id);

			var userRoles = await _userManager.GetRolesAsync(user);
			if (userRoles.Any())
			{
				await _userManager.RemoveFromRolesAsync(user, userRoles);
			}

			await _context.Entry(user)
						  .Collection(u => u.TaskSections)
						  .Query()
						  .Include(ts => ts.TaskItems)
						  .LoadAsync();

			if (user.TaskSections != null)
			{
				foreach (var section in user.TaskSections)
				{
					if (section.TaskItems != null)
					{
						_context.TaskItems.RemoveRange(section.TaskItems);
					}
					_context.TaskSections.Remove(section);
				}
			}

			await _context.SaveChangesAsync();
			IdentityResult result = await _userManager.DeleteAsync(user);

			return result;
		}


		public async Task<GetUserModel> GetUser(Guid id)
		{
			GetUserModel? user = await _context.Users
				.Where(x => x.Id == id)
				.Select(x => new GetUserModel
				{
					Id = x.Id,
					FirstName = x.FirstName,
					LastName = x.LastName,
					Email = x.Email,
					Roles = _context.UserRoles
						.Where(ur => ur.UserId == x.Id)
						.Join(_context.Roles,
							  ur => ur.RoleId,
							  r => r.Id,
							  (ur, r) => r.Name)
						.ToList(),
                    GetTaskSectionModels = x.TaskSections.Select(ts => new GetTaskSectionModel
                    {
                        Id = ts.Id,
                        Name = ts.Name,
                        GetTaskItemModels = ts.TaskItems.Select(ti => new GetTaskItemModel
                        {
                            Id = ti.Id,
                            Name = ti.Name
                        }).ToList()
                    }).ToList()
                })
				.AsNoTracking()
				.FirstOrDefaultAsync();

			return user;
		}

		public async Task<List<GetUserModel>> GetUsers()
		{
			ClaimsPrincipal _user = _httpContextAccessor.HttpContext!.User;
			List<GetUserModel> getUser = new();

			List<GetUserModel> users = await _context.Users
				.Select(x => new GetUserModel
				{
					Id = x.Id,
					FirstName = x.FirstName,
					LastName = x.LastName,
					Email = x.Email,
					Roles = _context.UserRoles
						.Where(ur => ur.UserId == x.Id)
						.Join(_context.Roles,
							  ur => ur.RoleId,
							  r => r.Id,
							  (ur, r) => r.Name)
						.ToList(),
                    GetTaskSectionModels = x.TaskSections.Select(ts => new GetTaskSectionModel
                    {
                        Id = ts.Id,
                        Name = ts.Name,
                        GetTaskItemModels = ts.TaskItems.Select(ti => new GetTaskItemModel
                        {
                            Id = ti.Id,
                            Name = ti.Name
                        }).ToList()
                    }).ToList()
                })
				.AsNoTracking()
				.ToListAsync();

			if (_user.IsInRole("admin"))
			{
				getUser = users.FindAll(x => x.Id.ToString() == _user.Identity!.Name);
			}
			else
			{
				getUser = users;
			}

			return getUser;
		}

		public async Task PatchUser(string id, PatchUserModel patchUserModel)
		{
			User user = await _userManager.FindByIdAsync(id);
			IdentityResult result = await _userManager.ChangePasswordAsync(user, patchUserModel.CurrentPassword, patchUserModel.NewPassword);

		}

		public async Task<GetUserModel> PostUser(PostUserModel postUserModel)
		{
			User user = new User
			{
				FirstName = postUserModel.FirstName,
				LastName = postUserModel.LastName,
				Email = postUserModel.Email,
				UserName = postUserModel.Email
			};

			IdentityResult result = await _userManager.CreateAsync(user, postUserModel.Password);
			if (result.Succeeded)
			{
				if (_context.Users.Count() == 1)
				{
					await _userManager.AddToRoleAsync(user, "admin");
				}
				else
				{
					await _userManager.AddToRoleAsync(user, "user");
				}

			}

			return new GetUserModel
			{
				Id = user.Id,
				FirstName = postUserModel.FirstName,
				LastName = postUserModel.LastName
			};
		}


		public async Task<bool> PutUser(string id, PutUserModel putUserModel)
		{
			User? user = await _userManager.FindByIdAsync(id);
			user.FirstName = putUserModel.FirstName;
			user.LastName = putUserModel.LastName;

			if (user == null)
			{
				return false;
			}
			IdentityResult result = await _userManager.UpdateAsync(user);
			return true;
		}

		private async Task<string> GenerateJwtToken(User user)
		{
			var roleNames = await _userManager.GetRolesAsync(user).ConfigureAwait(false);

			List<Claim> claims = new()
			{
				new Claim(ClaimTypes.Name, user.Id.ToString()),
				new Claim("FirstName", user.FirstName),
				new Claim("LastName", user.LastName),
				new Claim("UserName", user.UserName)
			};

			foreach (string roleName in roleNames)
			{
				claims.Add(new Claim(ClaimTypes.Role, roleName));
			}

			JwtSecurityTokenHandler tokenHandler = new();
			byte[] key = Encoding.ASCII.GetBytes(_appSettings.Secret);
			SecurityTokenDescriptor tokenDescriptor = new()
			{
				Issuer = "DomeBlazor WEB API",
				Subject = new ClaimsIdentity(claims.ToArray()),
				Expires = DateTime.UtcNow.AddHours(1),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};
			SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
			return tokenHandler.WriteToken(token);
		}

		private RefreshToken GenerateRefreshToken(string ipAddress)
		{
			byte[] randomBytes = RandomNumberGenerator.GetBytes(64);

			return new RefreshToken
			{
				Token = Convert.ToBase64String(randomBytes),
				Expires = DateTime.UtcNow.AddMinutes(2),
				Created = DateTime.UtcNow,
				CreatedByIp = ipAddress,
				RevokedByIp = ipAddress,
				ReplacedByToken = "dummyInit"
			};
		}
	}
}
