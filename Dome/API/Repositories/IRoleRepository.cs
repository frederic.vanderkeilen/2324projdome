﻿using Globals.Models.Role;
using Globals.Models.User;

namespace API.Repositories
{
    public interface IRoleRepository
    {
        Task<List<GetRoleModel>> GetRoles();
        Task<GetRoleModel> GetRole(Guid id);
        Task<GetRoleModel> PostRole(PostRoleModel postRoleModel);
        Task PutRole(string id, PutRoleModel putRoleModel);
        Task PatchRole(string id, PatchRoleModel patchRoleModel);
        Task DeleteRole(string id);
    }
}
