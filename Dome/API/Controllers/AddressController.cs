﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using API.Entities;
using API.Repositories;
using Microsoft.AspNetCore.Mvc;
using Globals;
using Globals.Models.User;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AddressController : ControllerBase
	{
		private readonly IAddressRepository _addressRepository;

		public AddressController(IAddressRepository addressRepository)
		{
			_addressRepository = addressRepository;
		}

		[HttpGet("/api/getaddress")]
		public async Task<Address> Get()
		{
			return await _addressRepository.GetAddress();
		}

		[HttpPost("/api/postaddress")]
		public async Task<ActionResult<Address>> Post(Address address)
		{
			Address _address = await _addressRepository.PostAddress(address);
			return _address;
		}
	}
}
