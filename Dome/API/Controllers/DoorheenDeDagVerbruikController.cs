﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using API.Entities;
using API.Repositories;
using Microsoft.AspNetCore.Mvc;
using Globals;

namespace API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class DoorheenDagVerbruikController : ControllerBase
	{
		private readonly IDoorheenDeDagVerbruikRepository _doorheenDeDagVerbruikRepository;

		public DoorheenDagVerbruikController(IDoorheenDeDagVerbruikRepository doorheenDeDagVerbruikRepository)
		{
			_doorheenDeDagVerbruikRepository = doorheenDeDagVerbruikRepository;
		}

		[HttpGet("/api/energyusage")]
		public async Task<List<DoorheenDeDagVerbruik>> Get()
		{
			return await _doorheenDeDagVerbruikRepository.GetEnergyUsage();
		}
	}
}
