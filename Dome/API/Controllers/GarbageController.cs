﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using API.Entities;
using API.Repositories;
using Globals;
using Globals.Models.TaskItem;
using Globals.Models.TaskSection;
using Globals.Models.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class GarbageController : ControllerBase
	{
		private readonly HttpClient _httpClient;
		private readonly CookieContainer _cookieContainer;
		private string sessionCookie;
		private readonly DataGarbage dataGarbage;
		private readonly DataBaseContext _context;
		private readonly IGarbageRepository _garbageRepository;

		public GarbageController(IGarbageRepository garbageRepository)
		{
			_cookieContainer = new CookieContainer();
			var handler = new HttpClientHandler
			{
				CookieContainer = _cookieContainer
			};
			_httpClient = new HttpClient(handler);
			dataGarbage = new DataGarbage();
			_garbageRepository = garbageRepository;
		}

		[HttpGet("/api/garbagecollection")]
		public async Task<IActionResult> Get(string ivagoStreet, string number)
		{
			try
			{
				var tasks = await _garbageRepository.GetGarbage();
				if (tasks.Count != 0)
				{
					Console.WriteLine("using local db");
					return Ok(tasks);
				}
				var response = await GetGarbageCollectionDataAsync(ivagoStreet, number);
				await _garbageRepository.PostGarbage(response);
				return Ok(response);
			}
			catch (HttpRequestException httpEx)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, $"Error fetching garbage collection data: {httpEx.Message}");
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, $"Unexpected error: {ex.Message}");
			}
		}

		private async Task<List<WasteCollectionTask>> GetGarbageCollectionDataAsync(string ivagoStreet, string number)
		{
			try
			{
				await dataGarbage.GetSessionCookieAsync(ivagoStreet, number);
				this.sessionCookie = dataGarbage.sessionCookie;
				if (string.IsNullOrEmpty(sessionCookie))
				{
					return new List<WasteCollectionTask>();
				}

				// Add the session cookie to the CookieContainer
				_cookieContainer.Add(new Uri("https://www.ivago.be"),
					new Cookie("SSESScb58c787e2bf9fd407e19d9a2a0995a5", sessionCookie));

				// Create the HTTP request
				var request = new HttpRequestMessage(HttpMethod.Get, "https://www.ivago.be/nl/particulier/garbage/pick-up/pickups?_format=json&type=&start=1704064210&end=1735671600");
				request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				request.Headers.Add("sec-fetch-site", "same-origin");
				request.Headers.Add("sec-fetch-mode", "cors");

				var response = await _httpClient.SendAsync(request);
				response.EnsureSuccessStatusCode();

				var jsonResponse = await response.Content.ReadAsStringAsync();
				var wasteCollectionTasks = JsonSerializer.Deserialize<List<WasteCollectionTask>>(jsonResponse, new JsonSerializerOptions
				{
					PropertyNameCaseInsensitive = true
				});

				return wasteCollectionTasks ?? new List<WasteCollectionTask>();
			}
			catch (Exception ex)
			{
				return new List<WasteCollectionTask>();
			}
		}

		[HttpPost("/api/postgarbage")]
		public async Task<ActionResult<Address>> Post(List<WasteCollectionTask> _tasks)
		{
			List<WasteCollectionTask> tasks = await _garbageRepository.PostGarbage(_tasks);
			return Ok(tasks);
		}
	}
}
