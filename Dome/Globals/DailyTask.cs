﻿using System.Text.Json.Serialization;

namespace Globals
{
    public abstract class DailyTask
    {
        [JsonPropertyName("date")]
        public DateTime Datum {  get; set; }
    }

    public class WasteCollectionTask : DailyTask
    {
        [JsonPropertyName("label")]
        public string Label { get; set; }
    }
}
