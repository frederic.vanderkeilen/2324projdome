﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals.Models.User
{
    public class PostDeactivationTokenRequestModel
    {
        public string Token { get; set; }
    }
}
