﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Globals.Models.User;

namespace DomeBlazor.Services
{

    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly IJSRuntime _jsRuntime;
        private readonly HttpClient _httpClient;

        public PostAuthenticationResponseModel responseModel { get; private set; }

        public CustomAuthenticationStateProvider(IJSRuntime jsRuntime, HttpClient httpClient)
        {
            _jsRuntime = jsRuntime;
            _httpClient = httpClient;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var authToken = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", "authToken");
            var identity = new ClaimsIdentity();

            if (!string.IsNullOrEmpty(authToken))
            {
                try
                {
                    var claims = ParseClaimsFromJwt(authToken);
                    identity = new ClaimsIdentity(claims, "jwt");
                    responseModel = new PostAuthenticationResponseModel
                    {
                        Id = identity.FindFirst("Id")?.Value,
                        FirstName = identity.FindFirst("FirstName")?.Value,
                        LastName = identity.FindFirst("LastName")?.Value,
                        UserName = identity.FindFirst("Username")?.Value,
                        JwtToken = authToken,
                        Roles = new List<string>(identity.FindAll(ClaimTypes.Role).Select(c => c.Value))
                    };
                }
                catch
                {
                    // Token is invalid, remove it
                    await _jsRuntime.InvokeVoidAsync("localStorage.removeItem", "authToken");
                }
            }

            var user = new ClaimsPrincipal(identity);
            return new AuthenticationState(user);
        }

        public async Task MarkUserAsAuthenticated(PostAuthenticationResponseModel responseModel)
        {
            await _jsRuntime.InvokeVoidAsync("localStorage.setItem", "authToken", responseModel.JwtToken);
			await _jsRuntime.InvokeVoidAsync("localStorage.setItem", "UserId", responseModel.Id);

			var claims = ParseClaimsFromJwt(responseModel.JwtToken);
            var identity = new ClaimsIdentity(claims, "jwt");
            var user = new ClaimsPrincipal(identity);
            responseModel = new PostAuthenticationResponseModel
            {
                Id = identity.FindFirst("Id")?.Value,
                FirstName = identity.FindFirst("FirstName")?.Value,
                LastName = identity.FindFirst("LastName")?.Value,
                UserName = identity.FindFirst("Username")?.Value,
                JwtToken = responseModel.JwtToken,
                Roles = new List<string>(identity.FindAll(ClaimTypes.Role).Select(c => c.Value))
            };
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        public async Task MarkUserAsLoggedOut()
        {
            await _jsRuntime.InvokeVoidAsync("localStorage.removeItem", "authToken");
            responseModel = null;
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()))));
        }

        private IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();

            if (handler.CanReadToken(jwt))
            {
                var token = handler.ReadJwtToken(jwt);
                return token.Claims;
            }
            else
            {
                throw new ArgumentException("The token is not a well-formed JWT.");
            }
        }
    }

}